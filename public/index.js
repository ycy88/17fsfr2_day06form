//IIFE 

(function (){
  var MyApp = angular.module("MyApp",[]);

  var MyController = function () {
    myController = this; // points to new controller object
    myController.name = "";
    myController.email = "";
    myController.phone = "";
    myController.comment = "";
    myController.exp = "";

    myController.clearForm = function () {
      myController.name = "";
      myController.email = "";
      myController.phone = "";
      myController.comment = "";
      myController.exp = "";
    };

  }
  // assign controller to string 'MyController'
  MyApp.controller('MyController', MyController)
})();