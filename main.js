const express = require ('express');

// new instance of express 
var app = express();

// routes

var staticResources = ["/public", "/bower_components"];
for (var i in staticResources) {
  app.use(express.static(__dirname + staticResources[i]));
}

// ports 
var port = process.argv[2] || 3000;
app.listen(port, function(){
  console.log('Express app now listening on port '+port);
});
